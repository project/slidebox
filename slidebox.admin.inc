<?php

/**
 * @file
 * Contains administrative forms and callbacks for Slidebox.
 */

/**
 * Menu callback. Displays the administration settings.
 */
function slidebox_admin_settings() {
  $form = array();

  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slidebox Display Options'),
    '#collapsible' => FALSE,
  );

  $form['display']['slidebox_show_time'] = array(
    '#title' => t('Slidebox Show Time'),
    '#description' => t('Number of miliseconds during which Slidebox should slide out'),
    '#type' => 'textfield',
    '#default_value' => variable_get('slidebox_show_time', 300),
  );

  $form['display']['slidebox_hide_time'] = array(
    '#title' => t('Slidebox Hide Time'),
    '#description' => t('Number of miliseconds during which Slidebox should slide back in'),
    '#type' => 'textfield',
    '#default_value' => variable_get('slidebox_hide_time', 100),
  );

  $form['display']['slidebox_trigger'] = array(
    '#title' => t('Slidebox Trigger'),
    '#description' => t('jQuery selector to trigger the Slidebox. The default typically results in the Slidebox appearing at the end of the page.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('slidebox_trigger', '#slidebox_trigger'),
  );

  $form['display']['slidebox_position'] = array(
    '#title' => t('Slidebox Trigger'),
    '#description' => t('Which side should the Slidebox come out from?'),
    '#type' => 'select',
    '#default_value' => variable_get('slidebox_position', 'right'),
    '#options' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
  );

  return system_settings_form($form);
}
